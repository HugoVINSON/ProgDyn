/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdlib.h>

#include "array_list.h"

/**
 * \brief   initialise la liste
 * \param   Array_list  structure  
 * */
void init_array_list(Array_list *l) {
    l->index = 0;
}

/**
 * \brief   Insert une valeur dans la liste à un indice donné
 * \param   Array_list  structure
 * \param   int     position
 * \param   float   value 
 * */

void insert_at(Array_list *l, int position, float value) {
    if (position < ARRAY_LIST_MAX_SIZE)
    {
        l->index++;
        for (int i = l->index; i > position; i--)
        {
            l->data[i] = l->data[i-1];
        }
        l->data[position] = value;   
    }
}

/**
 * \brief   Insert une valeur au dernier indice de la liste
 * \param   Array_list  structure
 * \param   float   value  
 * */
void add(Array_list *l, float value) {
    l->data[l->index++] = value;
}

/**
 * \brief   Supprime une valeur à l'indice donné
 * \param   Array_list  structure
 * \param   int     position
 * \return  float   value  
 * */
float remove_at(Array_list *l, int position) {
    if (position < ARRAY_LIST_MAX_SIZE)
    {
        float value = l->data[position];
        for (int i = position; i < l->index; i++)
        {
            l->data[i] = l->data[i+1];
        }
        l->index--;
        return value;
    }
    return -1;
}

/**
 * \brief   Retourne la valeur à l'indice donné
 * \param   Array_list  structure
 * \param   int     position
 * \return  float   valeur supprimée  
 * */
float get_at(Array_list *l,int position) {
    if (position < ARRAY_LIST_MAX_SIZE)
    {
        return l->data[position];
    }
}

/**
 * \brief   Retourne la valeur à l'indice donné
 * \param   Array_list  structure
 * */
void clear(Array_list *l) {
    l->index = 0;
    free(l);
}