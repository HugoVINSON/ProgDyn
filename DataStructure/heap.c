/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "heap.h"

/**
* \brief    Initialise la structure tas
* \param    Heap   structure
**/
void init_heap(Heap *h) { 
    h->index = 0;
}
/**
* \brief    Vérifie si le tas est vide 
* \param    Heap   structure
* \return   bool    1 si le tas est vide et 0 sinon
**/
bool is_heap_empty(Heap *h) { // https://gist.github.com/obstschale/5245140
    return h->index <= 0;
}

float pop_heap(Heap *h) { //pop the root value

} 
void push_heap(Heap *h, float value) {

}

float replace(Heap *h, float value) { // pop root and push a new key.

} 
float peek_heap(Heap *h) { // return root value but dont remove it

} 

//En option

// void heapify(Heap *s, float array[], size_t array_size) { //create Heap from array

// } 
// void merge(Heap *dest, Heap *src) { //merge src into dest

// } 