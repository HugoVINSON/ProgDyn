/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: aurelio
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include "stack.h"
#include "array_list.h"
#include "heap.h"
#include "queue.h"

// float test(int a) {
//     errno = a;
//     assert(a == 5);
//     int errnum = errno;
//     fprintf(stderr, "Value of errno: %d\n", errnum);
//     perror("Error printed by perror");
//     fprintf(stderr, "Error opening file: %s\n", strerror(errnum));

// }


/**
 * \brief   Fonction qui réalise les tests sur la structure "stack".
 **/
void test_stack() {
    Stack *s = malloc(sizeof(Stack));
    init_stack(s);
    push_stack(s, 12);
    push_stack(s, 5);
    push_stack(s, 8);
    assert(peek_stack(s) == 8);
    pop_stack(s);
    assert(peek_stack(s) == 5);
    assert(is_stack_empty(s) == 0);
    swap(s);
    assert(peek_stack(s) == 12);
    clear_stack(s);
    assert(is_stack_empty(s) == 1);
}

/**
 * \brief   Fonction qui réalise les tests sur la structure "queue".
 **/
void test_queue() {
    Queue *q = malloc(sizeof(Queue));
    init_queue(q);
    enqueue(q, 2);
    enqueue(q, 30);
    assert(front(q) == 2);
    dequeue(q);
    assert(front(q) == 30);
    assert(is_queue_empty(q) == 0);
    dequeue(q);
    dequeue(q);
    assert(is_queue_empty(q) == 1);
    enqueue(q, 10);
    clear_queue(q);
    assert(is_queue_empty(q) == 1);
}

/**
 * \brief   Fonction qui réalise les tests sur la structure "array_list".
 **/
void test_list() {
    Array_list *l = malloc(sizeof(Array_list));
    init_array_list(l);
    insert_at(l, 0, 21);
    assert(get_at(l,0) == 21);
    add(l, 24);
    assert(get_at(l,1) == 24);
    insert_at(l, 0, 12);
    assert(get_at(l,0) == 12);
    assert(remove_at(l,2) == 24);
    assert(get_at(l,2) != 24);
    clear(l);
    assert(l->index == 0); 
}

/**
* \brief   Fonction qui exécute les test des strctures de données.
**/
int main(int argc, char** argv) { 
    
    test_stack();
    test_queue();
    test_list();

    return (EXIT_SUCCESS);
}