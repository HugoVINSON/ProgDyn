/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdlib.h>

#include "queue.h"

/**
* \brief    Initialise la structure pile
* \param    Queue   structure
**/
void init_queue(Queue *q) {
    q->index = 0;
    q->begin = 0;
}

/**
* \brief    Ajoute une valeur à la pile
* \param    Queue   structure
* \param    float   value
**/
void enqueue(Queue *q, float value) {
    if (q->index < QUEUE_MAX_SIZE)
    {
        q->data[q->index] = value;
        q->index++;
    }
}

/**
* \brief    Supprime une valeur à la pile
* \param    Queue   structure
* \return   float   Le nombre supprimé
**/
float dequeue(Queue *q) {
    if (q->index > 0)
    {
        if (q->begin >= q->index)
        {
            q->index = 0;
            q->begin = 0;
        }
        else
        {
            return q->data[q->begin++];
        }
    }
    return -1;
}

/**
* \brief    Supprime une valeur à la pile
* \param    Queue   structure
* \return   bool   1 si la pile est vide et 0 sinon
**/
bool is_queue_empty(Queue *q) {
    return (q->index == 0 && q->begin == 0);
}

/**
* \brief    Affiche la dernière valeur ajoutée à la pile
* \param    Queue   structure
* \return   float   La dernière valeur de la pile
**/
float front(Queue *q) { //aka peek
    if (q->index > 0)
    {
        return q->data[q->begin];
    }
    return -1;
}

/**
* \brief    Nettoie la pile
* \param    Queue   structure
**/
void clear_queue(Queue *q) {
    q->index = 0;
    q->begin = 0;
    free(q);
}
