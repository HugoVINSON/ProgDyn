/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdlib.h>

#include "stack.h"

/**
* \brief    Initialise la structure file
* \param    Stack   structure
**/
void init_stack(Stack *s) {
    s->index = 0;
}

/**
* \brief    Ajoute une valeur à la file
* \param    Stack   structure
* \param    float   value
**/
void push_stack(Stack *s, float value) {
    if (s->index < STACK_MAX_SIZE)
    {
        s->data[s->index++] = value;
    }
}

/**
* \brief    Supprime la dernière valeur de la file
* \param    Stack   structure
* \return   float   la valeur supprimée   
**/
float pop_stack(Stack *s) {
    if (s->index > 0)
    {
        return s->data[--s->index];
    }
    return -1;
}

/**
* \brief    Vérifie si la file est vide
* \param    Stack   structure
* \return   bool    retourne 1 si la file est vide et 0 sinon
**/
bool is_stack_empty(Stack *s) {
    return s->index <= 0;
}

/**
* \brief    Retourne la dernière valeur de la file
* \param    Stack   structure
* \return   float   La dernière valeur de la file
**/
float peek_stack(Stack *s) { // aka top or front 
    if (s->index > 0)
    {
        return s->data[s->index-1];
    }
    return -1;
}

/**
* \brief    Duplique la dernière valeur de la file
* \param    Stack   structure
**/
void dup(Stack *s) {
    if (s->index > 0)
    {
        push_stack(s, peek_stack(s));
    }
}

/**
* \brief    Swap la dernière valeur avec la valeur d'avant
* \param    Stack   structure
**/
void swap(Stack *s) {
    float last, before_last;
    if (s->index > 1)
    {
        last = peek_stack(s);
        before_last = s->data[s->index-2];
        pop_stack(s);
        pop_stack(s);
        push_stack(s, last);
        push_stack(s, before_last);
    }
}

/**
* \brief    Nettoie la file
* \param    Stack   structure
**/
void clear_stack(Stack *s) {
    s->index = 0;
    free(s);
}