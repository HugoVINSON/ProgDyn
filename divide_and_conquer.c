#include <stdlib.h>
#include <stdbool.h>

#include "divide_and_conquer.h"

/**
 * \brief   Recherche par dichotomie dans un tableau d'entiers
 * \param   array   The array of values
 * \param   size_t  The size of the array
 * \param   value   The value to find
 * \return  The position of the value found or -1 
 * */
int find_by_dichotomy(int array[], int size_t, int value) {
    int m = size_t/2, start = 0, end = size_t;
    bool find = false;
    
    while (find != true, start <= end)
    {
        m = (start + end) / 2;
        if (array[m] == value)
        {
            find = true;
            return m;
        }
        else if (value > array[m])
        {
            start = m+1;
        }
        else
        {
            end = m-1;
        }
    }
}