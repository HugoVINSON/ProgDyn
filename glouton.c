#include <stdlib.h>
#include <stdio.h>

#include "glouton.h"

int indice = 0;

/**
 * \brief   Initialise un produit
 * \param   p       Tableau de produit
 * \param   poids   Le poids du produit
 * \param   prix    Le prix du produit
 * \param   indice  Permet de récupérer la taille du tableau
 * */
void initProduct(Produit *p, int poids, int prix) {
    p[indice].w = poids;
    p[indice].c = prix;
    p[indice].res = (float)prix/poids;
    indice++;
}

/**
 * \brief   Trie le tableau de produits dans l'ordre décroissant.
 * \return  Le tableau de produit trié.
 * */
Produit* sortList (Produit *p) {
    for (int i = 0; i < indice; i++)
    {
        for (int j = 0; j < indice ; j++)
        {
            if (p[j].res < p[j+1].res)
            {
                Produit temp = p[j];
                p[j] = p[j+1];
                p[j+1] = temp;
            }
        }
    }
    return p;
}

/**
 * \brief   Rempli le sac
 * \param   sac     Tableau de produit qui représente le sac
 * \param   p       Tableau de produit qui représente la liste des produits
 * \param   poids   Poids maximum que le sac peut contenir
 * */
Produit* rempliSac (Produit *sac, Produit *p, int poids) { //Vérifier qu'il n'y a pas un soucis avec les ints (précision)
    int maxBag = 0;
    int y=0;
    for (int i = 0; i <= indice; i++)
    {
        if (maxBag + p[i].w <= poids)
        {
            sac[y] = p[i];
            maxBag += p[i].w;
            y++;
        }
    }
}