typedef struct {
    int c;
    int w;
    float res;
} Produit;

void initProduct(Produit *p, int poids, int prix);
Produit* sortList (Produit *p);
Produit* rempliSac (Produit *sac, Produit *p, int poids);