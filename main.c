#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include "divide_and_conquer.h"
#include "glouton.h"
#include "plus_grand_carre.h"

/**
 * \brief   Fonction qui test la recherche par dichotomie.
 * */
void test_dichotomy() {
    
    int tableau[100], j = 1;
    for (int i = 0; i < 100; i++)
    {
        tableau[i] = j;
        j += 2;
    }
    assert(find_by_dichotomy(tableau, 100, 100) == 50);
}

/**
 * \brief   Fonction qui test l'algorithme glouton.
 * */
void test_knapsack_problem() {
    Produit *p = malloc(sizeof(Produit)*100);
    Produit *sac = malloc(sizeof(Produit)*100);
    initProduct(p, 12, 4);
    initProduct(p, 8, 4);
    initProduct(p, 5, 8);
    initProduct(p, 5, 8);
    initProduct(p, 2, 1);
    initProduct(p, 8, 8);
    initProduct(p, 15, 8);
    initProduct(p, 4, 12);
    initProduct(p, 3, 1);
    sortList(p);

    rempliSac(sac, p, 20);
    
    assert((sac[0].w == 4));
    assert((sac[1].w == 5));
    assert((sac[2].w == 5));
    assert((sac[3].w == 2));
    assert((sac[4].w == 3));
    assert((sac[5].w == 0));
    
    int totalPoids = 0;
    for (int z = 0; z < 100; z++)
    {
        totalPoids+= sac[z].w;
    }
    assert(totalPoids < 20);

    /* L'algorithme n'est pas optimal pour le jeu de valeur 
    (W = 10) (w1 = 6; w2 = 5; w3 = 5) (c1 = 7; c2 = 5; c3 = 5),
    */
}

void test_plus_grand_carre() {
    // int** mat = carre(3);
    // for (int i = 0; i <= 3; i++)
    // {
    //     for (int j = 0; j <= 3; j++) {
    //         printf("%d ",mat[i][j]);
    //     }
    // }
}

/**
 * \brief   Fonction qui appelle tous les tests du projet.
 * */
int main(int argc, char** argv) { 
    test_dichotomy();
    test_knapsack_problem();
    test_plus_grand_carre();

    return (EXIT_SUCCESS);
}